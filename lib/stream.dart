import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';


class StreamSend {
  Stream<String> getImage() async* {
    Future<List<String>> fList = fetchImage();
    List<String> tempo = await fList;

    yield* Stream.periodic(Duration(seconds:5), (int i) {
      int index = i % tempo.length;
      print(tempo[index]);
      return tempo[index];
    });
  }

  Future<List<String>> fetchImage() async {
    List<String> img = [];
    var urlcomponent= "https://thachln.github.io/toeic-data/ets/2016/1/p1/";
    var url = Uri.parse('https://thachln.github.io/toeic-data/ets/2016/1/p1/data.json');
    var response = await http.get(url);
    var jsonData = response.body;
    var jsonObject = jsonDecode(jsonData);
    for( int i = 0 ; i < jsonObject.length; i++){
      img.add(
          urlcomponent+jsonObject[i]['image']
      );
    }
    return img;
  }
}